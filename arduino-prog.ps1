#!/usr/bin/env pwsh
#--------------------------------------------------------------
#
#    This Pwershell Script uses forward declarations as follows.
#
#    #---------------------------------------------------------
#    # Main Function
#    #---------------------------------------------------------
#    function main
#    {
#        helper_function_1        # Calling helper_function_1
#
#        helper_function_2        # Calling helper_function_2
#    }
#
#    #---------------------------------------------------------
#    # Helper Function 1
#    #---------------------------------------------------------
#    function helper_function_1
#    {
#        do something
#    }
#    #---------------------------------------------------------
#    # Helper Function 2
#    #---------------------------------------------------------
#    function helper_function_1
#    {
#        do something else
#    }
#
#    #---------------------------------------------------------
#    # Main      The script entry point
#    #---------------------------------------------------------
#    main                      # Call function_main
#--------------------------------------------------------------

#---------------------------------------------------------
# Global Parameters
#---------------------------------------------------------
#---------------------------------------------------------------------------------
param([string]$DRIVE = "DRIVE", [string]$FOLDER = "FOLDER", [string]$VERSION = "VERSION", [string]$VENDOR = "VENDOR", [string]$BOARD = "BOARD",
      [string]$CPU = "CPU", [string]$SKETCH = "SKETCH", [string]$SKETCH_VER = "SKETCH_VER", [string]$ARCHITECTURE = "ARCHITECTURE",
	  [string]$PROTOCOL = "PROTOCOL" ) ;


#Read-Host -Prompt "Pausing:  Press any key to continue"

$BASE_PATH = "${DRIVE}/${FOLDER}"
$ARDUINO_PATH = "$BASE_PATH/arduino-$VERSION"
$PREFS_PATH = "$ARDUINO_PATH/portable"
$SKETCH_PATH= "$PREFS_PATH/sketchbook/arduino"
$BUILD_PATH= "${SKETCH_PATH}/${VENDOR}-${BOARD}-${CPU}-${SKETCH}-${SKETCH_VER}"
$USER="$env:USER"                                 # $env:VAR_NAME="VALUE"
$PROFILE_PATH = "/home/$env:USER"
$SCRIPT_PATH= "${DRIVE}/bin/pwshell"
$OPENOCD_PATH="${BASE_PATH}/openocd/bin"
#Read-Host -Prompt "Pausing:  Press any key to continue"



#---------------------------------------------------------
# Main Function
#---------------------------------------------------------
function main
{
    Write-Host "Line $(CurrentLine)   Entering               main"

    Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------"
    Write-Host "Line $(CurrentLine)   Calling                echo_args"
    echo_args                                                                        # Calling echo_args
#Read-Host -Prompt "Pausing:  Press any key to continue"

    echo "Line $(CurrentLine)   Calling                Launch_jtagit_prog()"
    Launch_jtagit_prog

}

#---------------------------------------------------------
# Echo Args
#---------------------------------------------------------
function echo_args
{
#$PREFS_PATH = "W:/"
$MyVariable = 1

    Write-Host "Line $(CurrentLine)   Entering               echo_args"
    Write-Host -NoNewline "Line $(CurrentLine)   Arguments              ";
    Write-Host -NoNewline "DRIVE = $DRIVE, "
    Write-Host -NoNewline "FOLDER = $FOLDER, "
    Write-Host -NoNewline "VERSION = $VERSION, "
    Write-Host -NoNewline "VENDOR = $VENDOR, "
    Write-Host "Line $(CurrentLine)  BASE_PATH            = $BASE_PATH";            
    Write-Host "Line $(CurrentLine)  BASE_PATH            = $BASE_PATH";            
    Write-Host "Line $(CurrentLine)  ARDUINO_PATH         = $ARDUINO_PATH";            
    Write-Host "Line $(CurrentLine)  PREFS_PATH           = $PREFS_PATH";            
    Write-Host "Line $(CurrentLine)  SKETCH_PATH          = $SKETCH_PATH";            
    Write-Host "Line $(CurrentLine)  BUILD_PATH           = $BUILD_PATH";            
    Write-Host "Line $(CurrentLine)  ARDUINO_PATH         = $ARDUINO_PATH";            
    Write-Host "Line $(CurrentLine)  OPENOCD_PATH         = $OPENOCD_PATH";            
    Write-Host "Line $(CurrentLine)  SKETCH               = $SKETCH";            
    Write-Host "Line $(CurrentLine)  Leaving                echo_args"
}

#---------------------------------------------------------
# Edit Profile
#---------------------------------------------------------
function Edit_Profile
{
    Write-Host "Line $(CurrentLine)  Entering               Edit_Profile()"

    Write-Host "Line $(CurrentLine)  Editing                .profile and  replace NEW_VERSION with $VERSION"
    ((Get-Content -path $PROFILE_PATH/.profile -Raw) -replace 'NEW_VERSION', $VERSION) | Set-Content -Path $PROFILE_PATH/.profile

    Write-Host "Line $(CurrentLine)  Leaving                Edit_Profile()"
}

#---------------------------------------------------------
# create_openocd_target
#---------------------------------------------------------
function create_openocd_target
{
    Write-Host "Line $(CurrentLine)  Entering               create_openocd_target()"

    Write-Host "Line $(CurrentLine)  Leaving                create_openocd_target()"
}

#---------------------------------------------------------
# Launch launcf_jtagit_prog
#---------------------------------------------------------
function launch_jtagit_prog
{
    Write-Host "Line $(CurrentLine)  Entering               Launch_jtagit_prog()"
Write-Host "Line $(CurrentLine)  Executing             'cd ${SKETCH_PATH}/${VENDOR}-${BOARD}-${CPU}-${SKETCH}-${SKETCH_VER}/build' "
    cd ${SKETCH_PATH}/${VENDOR}-${BOARD}-${CPU}-${SKETCH}-${SKETCH_VER}/build
    Write-Host "Line $(CurrentLine)  PWD                  = $PWD "
#   ls -al
#echo $PATH
#Read-Host -Prompt "Pausing:  Press any key to continue"
    Write-Host "Line $(CurrentLine)  Protocol             = ${PROTOCOL} "
	Write-Host "Line $(CurrentLine)  Executing              '& ${OPENOCD_PATH}/openocd -f board/eicon/${VENDOR}-${BOARD}-${CPU}-${SKETCH}-${SKETCH_VER}.ino-${PROTOCOL}.cfg' "
    & ${OPENOCD_PATH}/openocd -f board/eicon/${VENDOR}-${BOARD}-${CPU}-${SKETCH}-${SKETCH_VER}.ino-${PROTOCOL}.cfg

Read-Host -Prompt "Pausing:  Press any key to continue"

    cd $SCRIPT_PATH
    Write-Host "Line $(CurrentLine)  PWD                  = $PWD "
    
    Write-Host "Line $(CurrentLine)  Leaving                launch_jtagit_prog"
}

#---------------------------------------------------------
# CurrentLine
#---------------------------------------------------------
function CurrentLine
{
    $MyInvocation.ScriptLineNumber
}


#---------------------------------------------------------
# Main Entry point
#---------------------------------------------------------
echo "Line $(CurrentLine)  Calling                main()"
main
