#!/usr/bin/env pwsh
#--------------------------------------------------------------
#    Desktop Shortcut Example
#    W:\ProgramFiles\PowerShell\7\pwsh.exe -ExecutionPolicy Bypass -File W:\DinRDuino\PwrShell\arduino_ide.ps1 W DinRDuino 1.8.15 Eicon BluePill F103C8T6 SerLed V1.0.1 stm32 swd
#
#    Launching the Arduino IDE in a fully configured way.
#    This Pwershell Script uses forward declarations as follows.
#
#    #---------------------------------------------------------
#    # Main Function
#    #---------------------------------------------------------
#    function main
#    {
#        function_1        # Calling function_1
#        function_2        # Calling function_2
#    }
#    #---------------------------------------------------------
#    # Function 1
#    #---------------------------------------------------------
#    function function_1
#    {
#        do something
#    }
#    #---------------------------------------------------------
#    # Function 2
#    #---------------------------------------------------------
#    function function_1
#    {
#        do something else
#    }
#    #---------------------------------------------------------
#    # Main      The script entry point
#    #---------------------------------------------------------
#    main                      # Call function_main
#--------------------------------------------------------------

#---------------------------------------------------------
# Global Parameters
#---------------------------------------------------------

$OS                            = "WSL"
$OS_LC                         = "wsl"
$USER                          = "eicon"
$BASE_ROOT                     = "/home/${USER}"
$INSTALL_FOLDER                = "J-Tagit"
$WIN_ROOT                      = "/mnt/c"
$ARDUINO_VERSION               = "1.8.18"
$ARDUINO_LIBRARIES_RELEASE     = "1.0.1"
$ARDUINO_CLI_RELEASE           = "0.20.2"
$BOARD_VENDOR_EI               = "Eicon"
$BOARD_VENDOR_EI_LC            = "eicon"
$BOARD_VENDOR_ST               = "STMicroelectronics"
$SKETCHBOOK_RELEASE            = "0.1.0"
$OPENOCD_RELEASE               = "0.11.0-1"
$OPENOCD_CFG_RELEASE           = "0.1.0"
$XPACK_GCC_VERSION             = "10.2.1-1.1"
$XPACK_GCC_NAME                = "xpack-arm-none-eabi-gcc"
$EICON_GCC_VERSION             = "5.4.1"
$CMSIS_VERSION                 = "5.7.0"
$CORE_NAME                     = "Arduino_Core_STM32"
$CORE_PLATFORM_NAME            = "stm32"
$CORE_RELEASE                  = "2.2.0"
$CPU                           = "F103CBT6"
$SKETCH                        = "HelloW"

$ARDUINO_TB_TYPE               = "linux64.tar.xz"
$OPENOCD_TB_TYPE               = "linux-x64.tar.gz"
$ARDUINO_CLI_TB_TYPE           = "Linux_64bit.tar.gz"
$ARDUINO_LIBRARIES_PREFIX      = "eicon-${OS_LC}-arduino-libraries"
$SKETCHBOOK_NAME_PREFIX        = "eicon-${OS_LC}-sketchbook"
$EICON_GCC_NAME                = "eicon-arm-none-eabi-gcc"
$ARDUINO_CLI_TB_NAME           = "arduino-cli_${ARDUINO_CLI_RELEASE}_${ARDUINO_CLI_TB_TYPE}"
$OPENOCD_CFG_SCRIPT_NAME       = "${Board_VENDOR_EI_LC}-${OS_LC}-openocd-cfg"
$BASE_PATH                     = "${BASE_ROOT}/${INSTALL_FOLDER}"
$ARDUINO_PATH                  = "${BASE_PATH}/arduino-${ARDUINO_VERSION}"
$PORTABLE_PATH                 = "${ARDUINO_PATH}/portable"
$HARDWARE_PATH_EI              = "${PORTABLE_PATH}/packages/${BOARD_VENDOR_EI}/hardware"
$HARDWARE_PATH_ST              = "${PORTABLE_PATH}/packages/${BOARD_VENDOR_ST}/hardware"
$VARIANTS_PATH_EI              = "${HARDWARE_PATH_EI}/${CORE_PLATFORM_NAME}/${CORE_RELEASE}"
$TOOLS_PATH_EI                 = "${PORTABLE_PATH}/packages/${BOARD_VENDOR_EI}/tools"
$TOOLS_PATH_ST                 = "${PORTABLE_PATH}/packages/${BOARD_VENDOR_ST}/tools"
$OPENOCD_PATH                  = "${BASE_PATH}/Openocd"
$DLOAD_PATH                    = "${BASE_PATH}/dload"
$PREFS_PATH                    = "${PORTABLE_PATH}"
$SKETCH_PATH                   = "${PORTABLE_PATH}/sketchbook/arduino"

$ARDUINO_ZIP_URI               = "https://downloads.arduino.cc/arduino-${ARDUINO_VERSION}-${ARDUINO_TB_TYPE}"
$ARDUINO_CLI_URI               = "https://downloads.arduino.cc/arduino-cli/arduino-cli_${ARDUINO_CLI_RELEASE}_${ARDUINO_CLI_TB_TYPE}"
$OPENOCD_CFG_SCRIPT_URI        = "https://gitlab.com/noeldiviney/${OPENOCD_CFG_SCRIPT_NAME}/-/archive/${OPENOCD_CFG_RELEASE}/${OPENOCD_CFG_SCRIPT_NAME}-${OPENOCD_CFG_RELEASE}.tar.gz"
$ARDUINO_LIBRARIES_ZIP_URI     = "https://gitlab.com/noeldiviney/${BOARD_VENDOR_EI_LC}-${OS_LC}-arduino-libraries/-/archive/${ARDUINO_LIBRARIES_RELEASE}/${ARDUINO_LIBRARIES_PREFIX}-${ARDUINO_LIBRARIES_RELEASE}.zip"
$SKETCHBOOK_ZIP_URI            = "https://gitlab.com/noeldiviney/${BOARD_VENDOR_EI_LC}-${OS_LC}-sketchbook/-/archive/${SKETCHBOOK_RELEASE}/${BOARD_VENDOR_EI_LC}-${OS_LC}-sketchbook-${SKETCHBOOK_RELEASE}.zip"

$VARIANTS_URI                  = "https://gitlab.com/noeldiviney/${BOARD_VENDOR_EI_LC}-${OS_LC}-${CORE_PLATFORM_NAME}-variants/-/archive/${CORE_RELEASE}/${BOARD_VENDOR_EI_LC}-${OS_LC}-${CORE_PLATFORM_NAME}-variants-${CORE_RELEASE}.zip"
$CORE_ZIP_URI                  = "https://github.com/stm32duino/${CORE_NAME}/archive/refs/tags/${CORE_RELEASE}.zip"
                                ## https://github.com/stm32duino/Arduino_Core_STM32/archive/refs/tags/2.2.0.zip

$ARDUINO_CORE_ZIP_NAME         = "${CORE_NAME}-${CORE_RELEASE}.zip"

$VARIANTS_ZIP_NAME             = "${BOARD_VENDOR_EI_LC}-${OS_LC}-${CORE_PLATFORM_NAME}-variants-${CORE_RELEASE}.zip"
$VARIANTS_NAME                 = "${BOARD_VENDOR_EI_LC}-${OS_LC}-${CORE_PLATFORM_NAME}-variants-${CORE_RELEASE}"
$OPENOCD_TB_NAME               = "xpack-openocd-${OPENOCD_RELEASE}-${OPENOCD_TB_TYPE}"
$OPENOCD_TB_URI                = "https://github.com/xpack-dev-tools/openocd-xpack/releases/download/v${OPENOCD_RELEASE}/xpack-openocd-${OPENOCD_RELEASE}-${OPENOCD_TB_TYPE}"
$STM32_INDEX                   = "        https://gitlab.com/noeldiviney/eicon_${OS_LC}_packages/-/raw/main/package_eicon_stm32_index.json,"
$KINETIS_INDEX                 = "        https://gitlab.com/noeldiviney/eicon_${OS_LC}_packages/-/raw/main/package_eicon_kinetis_index.json,"
$STMicroelectronics_INDEX      = "        https://github.com/stm32duino/BoardManagerFiles/raw/main/package_stmicroelectronics_index.json"

$ARDUINO_ZIP_FILE              = "arduino-${ARDUINO_VERSION}.zip"
$ARDUINO_LIBRARIES_ZIP_FILE    = "${BOARD_VENDOR_EI_LC}-${OS_LC}-arduino-libraries-${ARDUINO_LIBRARIES_RELEASE}.zip"
$SKETCH_NAME                   = "${CPU}-${SKETCH}"
$SCRIPT_PATH                   = "${BASE_ROOT}/bin"
$OPENOCD_ZIP_FILE              = "eicon-openocd-${OPENOCD_RELEASE}.zip"
$ARDUINO_CLI_ZIP_FILE          = "arduino_cli.zip"
$SKETCHBOOK_ZIP_FILE           = "sketchbook.zip"
$SourceFileLocation            = "${BASE_ROOT}Program Files/Notepad++/notepad++.exe"

Read-Host -Prompt "Pausing:  Press any key to continue"

#---------------------------------------------------------
# Main Function
#---------------------------------------------------------
function main
{
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------";
    Write-Host "Line $(CurrentLine)  Entering                        main"; 

#    Write-Host "Line $(CurrentLine)  Calling                      setup_os_stuff";
#    setup_os_stuff
#Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                         echo_args";
    echo_args                                                                        # Calling echo_args
Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"


    Write-Host "Line $(CurrentLine)  Calling                         create_${BASE_PATH}";
   create_${BASE_PATH}
#Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"
    
    Write-Host "Line $(CurrentLine)  Calling                         install_arduino-${VERSION}";
    install_arduino-${VERSION}
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    
##    Write-Host "Line $(CurrentLine)  Calling                         install_arduino-libraries";
##    install_arduino-libraries
##Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                         install_openocd-${OPENOCD_RELEASE}"
    install_openocd-${OPENOCD_RELEASE}
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                         install_openocd_cfg_scripts"
    install_openocd_cfg_scripts
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                         install_arduino_cli"
    install_arduino_cli
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                         install_arduino_cli_config"
    install_arduino_cli_config
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                         core_update-index"
    core_update-index
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    
#    Write-Host "Line $(CurrentLine)  Calling                         core_install_stm32"
#    core_install_stm32
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                         install_sketchbook"
    install_sketchbook
#Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                         install_udev_rules"
    install_udev_rules
#Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                         core_install_kinetis"
    core_install_kinetis
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                         core_install_stmicro"
    core_install_stmicro
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                         install_eicon_xpack"
    install_eicon_xpack
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                         reconfig_paths"
    reconfig_paths                                                                               # Disable for further testing
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                         install_arduino_core"
    install_arduino_core
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                         install_eicon_lin_stm32_variants"
    install_eicon_lin_stm32_variants
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                         install_skrtchbook"
    install_sketchbook
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                         install_cmake_tools"
    install_cmake_tools
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"


#    Write-Host "Line $(CurrentLine)  Calling                      install_DesktopShortcut"       # Work in Profress !!!
#    install_DesktopShortcut

#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
}

#---------------------------------------------------------
# install_DesktopShortcut
#---------------------------------------------------------
function install_DesktopShortcut
{
#$WScriptShell = New-Object -ComObject WScript.Shell
    Write-Host "Line $(CurrentLine)  Entering                     install_DesktopShortcut";
#    $SourceFileLocation = "C:/Program Files/PowerShell/7/pwsh.exe"
#    $ShortcutLocation = "C:/Users/$USER/Desktop/pwsh.exe.lnk"
#    $WScriptShell = New-Object -ComObject WScript.Shell
#    $Shortcut = "$WScriptShell.CreateShortcut($ShortcutLocation)" 
#    $Shortcut.TargetPath = $SourceFileLocation "C:/Program Files/PwrShell/arduino-install.ps1" "W Dinrduino5 1.8.15 Eicon"
#    $Shortcut.Save()
#    Write-Host "Line $(CurrentLine)  Executing                 $ShortcutLocation = "C:/Users/$USER/Desktop/Notepad++.lnk";
#    $ShortcutLocation = "C:/Users/$USER/Desktop/Notepad++.lnk"
#    Write-Host "Line $(CurrentLine)  Executing                 $WScriptShell = New-Object -ComObject WScript.Shell";                 
#    $WScriptShell = New-Object -ComObject WScript.Shell
#    Write-Host "Line $(CurrentLine)  Executing                 '$Shortcut = $WScriptShell.CreateShortcut($ShortcutLocation)'";
#    $Shortcut = $WScriptShell.CreateShortcut($ShortcutLocation)
#    Write-Host "Line $(CurrentLine)  Executing                 $Shortcut.TargetPath = $SourceFileLocation";
#    $Shortcut.TargetPath = $SourceFileLocation
#    Write-Host "Line $(CurrentLine)  Executing                 $Shortcut.Save()";
#    $Shortcut.Save()
    Write-Host "Line $(CurrentLine)  Leaving                      install_DesktopShortcut";
}

#---------------------------------------------------------
# install_udev_rules
#---------------------------------------------------------
function install_udev_rules
{
    Write-Host "Line $(CurrentLine)  Entering                     install_udev_rules";
    Write-Host "Line $(CurrentLine)  Executing                    sudo cp ~/bin/rules.d/99-libftdi.rules /etc/udev/rules.d/";                    
    sudo cp ~/bin/rules.d/99-libftdi.rules /etc/udev/rules.d/
    Write-Host "Line $(CurrentLine)  Executing                    sudo cp ~/bin/rules.d/99-openocd.rules /etc/udev/rules.d/";
    sudo cp ~/bin/rules.d/99-openocd.rules /etc/udev/rules.d/
    Write-Host "Line $(CurrentLine)  Executing                    sudo udevadm control --reload-rules";
    sudo udevadm control --reload-rules
    
    Write-Host "Line $(CurrentLine)  Leaving                      install_udev_rules";
}

#---------------------------------------------------------
# install_cmake_tools
#---------------------------------------------------------
function install_cmake_tools
{
    Write-Host "Line $(CurrentLine)  Entering                     install_cmake_tools";
    Write-Host "Line $(CurrentLine)  Exexcuting                   Copy-item -Force -Recurse ${TOOLS_PATH_EI} -Destination ${BASE_PATH} ";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Copy-item -Force -Recurse ${TOOLS_PATH_EI} -Destination ${BASE_PATH}
    Write-Host "Line $(CurrentLine)  Leaving                      install_cmake_tools";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# install_sketchbook
#---------------------------------------------------------
function install_sketchbook
{
    Write-Host "Line $(CurrentLine)  Entering                     install_sketchbook";
    Write-Host "Line $(CurrentLine)  Downloading                  ${BASE_PATH}/dload/${SKETCHBOOK_ZIP_FILE}";
    Write-Host "Line $(CurrentLine)  Executing                    Invoke-WebRequest -Uri ${SKETCHBOOK_ZIP_URI} -Outfile ${BASE_PATH}/dload/${SKETCHBOOK_ZIP_FILE}";
    Invoke-WebRequest -Uri ${SKETCHBOOK_ZIP_URI} -Outfile ${BASE_PATH}/dload/${SKETCHBOOK_ZIP_FILE}
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${PORTABLE_PATH}/sketchbook)";
    if (Test-Path ${PORTABLE_PATH}/sketchbook)
    {
        Write-Host "Line $(CurrentLine)  sketchbook folder            Exists , Deleting";
        Remove-Item -Recurse -Force ${PORTABLE_PATH}/sketchbook
    }	
    Write-Host "Line $(CurrentLine)  Executing                    Expand-Archive -PATH ${BASE_PATH}/dload/${SKETCHBOOK_ZIP_FILE} -Destination ${PORTABLE_PATH}/";
    Expand-Archive -Force -PATH ${BASE_PATH}/dload/${SKETCHBOOK_ZIP_FILE} -Destination ${PORTABLE_PATH}/ 
    Write-Host "Line $(CurrentLine)  Renaming                     ${PORTABLE_PATH}/${SKETCHBOOK_NAME_PREFIX}*";
    Write-Host "Line $(CurrentLine)  To                           ${PORTABLE_PATH}/sketchbook";
    Rename-Item ${PORTABLE_PATH}/${SKETCHBOOK_NAME_PREFIX}* ${PORTABLE_PATH}/sketchbook
    Write-Host "Line $(CurrentLine)  Leaving                      install_sketchbook";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# install_eicon_lin_stm32_variants
#---------------------------------------------------------
function install_eicon_lin_stm32_variants
{
    Write-Host "Line $(CurrentLine)  Entering                     install_eicon_win_stm32_variants";
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${VARIANTS_PATH_EI}/variants)";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${VARIANTS_PATH_EI}/variants)
    {
        Write-Host "Line $(CurrentLine)  variants                     exist  ...  deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                    Remove-Item -Recurse -Force ${VARIANTS_PATH_EI}/variants" ;  
        Remove-Item -Recurse -Force ${VARIANTS_PATH_EI}/variants
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    }
	else
    {
        Write-Host "Line $(CurrentLine)  variants                     do not exist  ...  continuing" ;  
    }


#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"


    if (Test-Path ${VARIANTS_PATH_EI}/boards.txt)
    {
        Write-Host "Line $(CurrentLine)  boards.txt                   exist  ...  deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                    Remove-Item -Recurse -Force ${VARIANTS_PATH_EI}/boards.txt" ;  
        Remove-Item -Recurse -Force ${VARIANTS_PATH_EI}/boards.txt
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    }
	else
    {
        Write-Host "Line $(CurrentLine)  boards.txt                   does not exist  ...  continuing" ;  
    }

    if (Test-Path ${VARIANTS_PATH_EI}/platform.txt)
    {
        Write-Host "Line $(CurrentLine)  platform.txt                 exist  ...  deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                    Remove-Item -Recurse -Force ${VARIANTS_PATH_EI}/platform.txt" ;  
        Remove-Item -Recurse -Force ${VARIANTS_PATH_EI}/platform.txt
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    }
	else
    {
        Write-Host "Line $(CurrentLine)  platform.txt                 does not exist  ...  continuing" ;  
    }

    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${DLOAD_PATH}/${VARIANTS_ZIP_NAME})";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${DLOAD_PATH}/${VARIANTS_ZIP_NAME})
    {
        Write-Host "Line $(CurrentLine)  variants zip             exists  ...  deleting" ;  
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Write-Host "Line $(CurrentLine)  Executing                    Remove-Item -Recurse -Force ${DLOAD_PATH}/${VARIANTS_ZIP_NAME}" ;  
        Remove-Item -Recurse -Force ${DLOAD_PATH}/${VARIANTS_ZIP_NAME}        
    }

#    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${DLOAD_PATH}/${VARIANTS_NAME})";
#    if (Test-Path ${DLOAD_PATH}/${VARIANTS_NAME})
#    {
#        Write-Host "Line $(CurrentLine)  variants unzipped            exists  ...  deleting" ;  
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
#        Write-Host "Line $(CurrentLine)  Executing                    Remove-Item -Recurse -Force ${DLOAD_PATH}/${VARIANTS_NAME}" ;  
#        Remove-Item -Recurse -Force ${DLOAD_PATH}/${VARIANTS_NAME}        
#    }
		
    Write-Host "Line $(CurrentLine)  Downloading                  ${DLOAD_PATH}/${VARIANTS_ZIP_NAME}";
    Write-Host "Line $(CurrentLine)  Executing                    Invoke-WebRequest -Uri ${VARIANTS_URI} -Outfile ${DLOAD_PATH}/${VARIANTS_ZIP_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Invoke-WebRequest -Uri ${VARIANTS_URI} -Outfile ${DLOAD_PATH}/${VARIANTS_ZIP_NAME}

    Write-Host "Line $(CurrentLine)  Executing                    Expand-Archive -Force -Path ${DLOAD_PATH}/${VARIANTS_ZIP_NAME} -DestinationPath ${DLOAD_PATH}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Expand-Archive -Force -Path ${DLOAD_PATH}/${VARIANTS_ZIP_NAME} -DestinationPath ${DLOAD_PATH}
	
    Write-Host "Line $(CurrentLine)  Executing                    Copy-Item -Recurse -Force -path: "${DLOAD_PATH}/${VARIANTS_NAME}/*" -destination: ${VARIANTS_PATH_EI}";
    Copy-Item -Recurse -Force -path: "${DLOAD_PATH}/${VARIANTS_NAME}/*" -destination: ${VARIANTS_PATH_EI}
	
    Write-Host "Line $(CurrentLine)  Leaving                      install_eicon_lin_stm32_variants";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}



#---------------------------------------------------------
# install_arduino_core
#---------------------------------------------------------
function install_arduino_core
{
    Write-Host "Line $(CurrentLine)  Entering                     install_arduino_core";


    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${DLOAD_PATH}/${ARDUINO_CORE_ZIP_NAME})";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${DLOAD_PATH}/${ARDUINO_CORE_ZIP_NAME})
    {
        Write-Host "Line $(CurrentLine)  Arduino STM32 Core           Exists" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Arduino STM32 Core           does not exist  ...   downloading" ;  
        Write-Host "Line $(CurrentLine)  Downloading                  ${DLOAD_PATH}/${ARDUINO_CORE_ZIP_NAME}";
        Write-Host "Line $(CurrentLine)  Executing                    Invoke-WebRequest -Uri ${CORE_ZIP_URI} -Outfile ${DLOAD_PATH}/${ARDUINO_CORE_ZIP_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Invoke-WebRequest -Uri ${CORE_ZIP_URI} -Outfile ${DLOAD_PATH}/${ARDUINO_CORE_ZIP_NAME}
    }
	
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${HARDWARE_PATH_EI}/${CORE_NAME})";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${HARDWARE_PATH_EI}/${CORE_NAME})
    {
        Write-Host "Line $(CurrentLine)  Arduino STM32 Core           exists  ...  deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                    Remove-Item -Recurse -Force ${HARDWARE_PATH_EI}/${CORE_NAME} ";  
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Remove-Item -Recurse -Force ${HARDWARE_PATH_EI}/${CORE_NAME}
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Arduino STM32 Core                       does not exist ...  continuing" ;  
    }

    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${HARDWARE_PATH_EI}/${CORE_PLATFORM_NAME})";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${HARDWARE_PATH_EI}/${CORE_PLATFORM_NAME})
    {
        Write-Host "Line $(CurrentLine)  stm32 Platform               is installed  ...  deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                    Remove-Item -Recurse -Force ${HARDWARE_PATH_EI}/${CORE_PLATFORM_NAME} ";  
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Remove-Item -Recurse -Force ${HARDWARE_PATH_EI}/${CORE_PLATFORM_NAME}
    }
    else
    {
        Write-Host "Line $(CurrentLine)  stm32 Platform                           is not installed ...  continuing" ;  
    }


    Write-Host "Line $(CurrentLine)  Executing                    Expand-Archive -Path ${DLOAD_PATH}/${ARDUINO_CORE_ZIP_NAME} -DestinationPath ${HARDWARE_PATH_EI}/${CORE_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Expand-Archive -Path ${DLOAD_PATH}/${ARDUINO_CORE_ZIP_NAME} -DestinationPath ${HARDWARE_PATH_EI}/${CORE_NAME}

#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Renaming                     ${HARDWARE_PATH_EI}/${CORE_NAME}";
    Write-Host "Line $(CurrentLine)  To                           ${HARDWARE_PATH_EI}/${CORE_PLATFORM_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Rename-Item  ${HARDWARE_PATH_EI}/${CORE_NAME}  ${HARDWARE_PATH_EI}/${CORE_PLATFORM_NAME}

    Write-Host "Line $(CurrentLine)  Renaming                     ${HARDWARE_PATH_EI}/${CORE_PLATFORM_NAME}/${CORE_NAME}-${CORE_RELEASE}";
    Write-Host "Line $(CurrentLine)  To                           ${HARDWARE_PATH_EI}/${CORE_PLATFORM_NAME}/${CORE_RELEASE}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Rename-Item  ${HARDWARE_PATH_EI}/${CORE_PLATFORM_NAME}/${CORE_NAME}-${CORE_RELEASE}  ${HARDWARE_PATH_EI}/${CORE_PLATFORM_NAME}/${CORE_RELEASE}

		
    Write-Host "Line $(CurrentLine)  Leaving                      install_arduino_core";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
#  reconfig_paths
#---------------------------------------------------------
function reconfig_paths
{
    Write-Host "Line $(CurrentLine)  Entering                     reconfig_paths";
    Write-Host "Line $(CurrentLine)  Exexcuting                   Get-ChildItem -Path ${TOOLS_PATH_EI}/CMSIS/*/ -Recurse | Move-Item  -Destination ${TOOLS_PATH_EI}/CMSIS/ ";
    Get-ChildItem -Path ${TOOLS_PATH_EI}/CMSIS/*/ -Recurse | Move-Item -Force -Destination ${TOOLS_PATH_EI}/CMSIS/

    Write-Host "Line $(CurrentLine)  Exexcuting                   Get-ChildItem -Path ${TOOLS_PATH_EI}/${XPACK_GCC_NAME}/*/ -Recurse | Move-Item  -Destination ${TOOLS_PATH_EI}/${XPACK_GCC_NAME}/ ";
    Get-ChildItem -Path ${TOOLS_PATH_EI}/${XPACK_GCC_NAME}/*/ -Recurse | Move-Item  -Force -Destination ${TOOLS_PATH_EI}/${XPACK_GCC_NAME}/

    Write-Host "Line $(CurrentLine)  Exexcuting                   Get-ChildItem -Path ${TOOLS_PATH_EI}/${EICON_GCC_NAME}/*/ -Recurse | Move-Item  -Destination ${TOOLS_PATH_EI}/${EICON_GCC_NAME}/ ";
    Get-ChildItem -Path ${TOOLS_PATH_EI}/${EICON_GCC_NAME}/*/ -Recurse | Move-Item -Force -Destination ${TOOLS_PATH_EI}/${EICON_GCC_NAME}/
    Write-Host "Line $(CurrentLine)  Leaving                      reconfig_paths";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
#  install_eicon_xpack
#---------------------------------------------------------
function install_eicon_xpack
{
    Write-Host "Line $(CurrentLine)  Entering                     install_eicon_xpack";
    Write-Host "Line $(CurrentLine)  Exexcuting                   Copy-item -Force -Recurse  ${TOOLS_PATH_ST}/${XPACK_GCC_NAME} -Destination ${TOOLS_PATH_EI}/${XPACK_GCC_NAME} ";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Copy-item -Force -Recurse ${TOOLS_PATH_ST}/${XPACK_GCC_NAME} -Destination ${TOOLS_PATH_EI}/${XPACK_GCC_NAME}
    Write-Host "Line $(CurrentLine)  Leaving                      install_eicon_xpack";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

function core_install_stmicro
{
    Write-Host "Line $(CurrentLine)  Entering                     core_install_stmicro";
    if (Test-Path ${ST_HARDWARE_PATH}/stm32)
    {
        Write-Host "Line $(CurrentLine)  stmicro_core                 Exists , Deleting";
        Remove-Item -Recurse -Force ${ST_HARDWARE_PATH_EI}/stm32
        Remove-Item -Recurse -Force ${TOOLS_PATH_EI}/${EICON_GCC_NAME}
    }
    Write-Host "Line $(CurrentLine)  Executing                    arduino-cli core install ${BOARD_VENDOR_ST}:stm32";
    & $PORTABLE_PATH/arduino-cli core install ${BOARD_VENDOR_ST}:stm32    
    Write-Host "Line $(CurrentLine)  Leaving                      core_install_micro";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

function core_install_kinetis
{
    Write-Host "Line $(CurrentLine)  Entering                     core_install_kinetis";
    if (Test-Path ${EI_HARDWARE_PATH}/kinetis)
    {
        Write-Host "Line $(CurrentLine)  kinetis_core                 Exists , Deleting";
        Remove-Item -Recurse -Force ${EI_HARDWARE_PATH}/kinetis
        Remove-Item -Recurse -Force ${EI_TOOLS_PATH_EI}/${EICON_GCC_NAME}
    }
    Write-Host "Line $(CurrentLine)  Executing                    arduino-cli core install ${BOARD_VENDOR_EI}:kinetis";
    & $PORTABLE_PATH/arduino-cli core install ${BOARD_VENDOR_EI}:kinetis    
    Write-Host "Line $(CurrentLine)  Leaving                      core_install_kinetis";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

function core_install_stm32
{
    Write-Host "Line $(CurrentLine)  Entering                     core_install_stm32";
    if (Test-Path ${EI_HARDWARE_PATH}/stm32)
    {
        Write-Host "Line $(CurrentLine)  core                         Exists , Deleting";
        Remove-Item -Recurse -Force ${EI_HARDWARE_PATH}/stm32
        Remove-Item -Recurse -Force ${TOOLS_PATH_EI}/CMSIS
        Remove-Item -Recurse -Force ${TOOLS_PATH_EI}/${XPACK_GCC_NAME}
    }
    Write-Host "Line $(CurrentLine)  Executing                    arduino-cli core install ${BOARD_VENDOR_EI}:stm32";
    & $PORTABLE_PATH/arduino-cli core install ${BOARD_VENDOR_EI}:stm32    
    Write-Host "Line $(CurrentLine)  Leaving                      core_install_stm32";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}
#Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"

#---------------------------------------------------------
# core_update-index
#---------------------------------------------------------
function core_update-index
{
    Write-Host "Line $(CurrentLine)  Entering                     core_update-index";
    Write-Host "Line $(CurrentLine)  Executing                    cd ${PORTABLE_PATH}";
    cd ${PORTABLE_PATH}
    Write-Host "Line $(CurrentLine)  Current Working Dir        = $PWD";
    Write-Host "Line $(CurrentLine)  Executing                    arduino-cli core update-index";
    & $PORTABLE_PATH/arduino-cli core update-index    
    Write-Host "Line $(CurrentLine)  Leaving                      core_update-index";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}
#---------------------------------------------------------
# install_arduino_cli_config
#---------------------------------------------------------
function install_arduino_cli_config
{
    Write-Host "Line $(CurrentLine)  Entering                     install_arduino_cli_config";

    if (Test-Path ${PORTABLE_PATH}/arduino-cli.yaml)
    {
        Write-Host "Line $(CurrentLine)  arduino-cli.yaml             Exists , Deleting";
        Remove-Item ${PORTABLE_PATH}/arduino-cli.yaml
        Write-Host "Line $(CurrentLine)  Executing                    New-Item ${PORTABLE_PATH}/arduino-cli.yaml";
        New-Item ${PORTABLE_PATH}/arduino-cli.yaml
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Executing                    New-Item ${PORTABLE_PATH}/arduino-cli.yaml";
        New-Item ${PORTABLE_PATH}/arduino-cli.yaml
    }    
    Write-Host "Line $(CurrentLine)  Adding                       arduino-cli.yaml Contents";
    Add-Content ${PORTABLE_PATH}/arduino-cli.yaml 'board_manager:'
    Add-Content ${PORTABLE_PATH}/arduino-cli.yaml '    additional_urls: ['
    Add-Content ${PORTABLE_PATH}/arduino-cli.yaml          ${STM32_INDEX}
    Add-Content ${PORTABLE_PATH}/arduino-cli.yaml          ${KINETIS_INDEX}
    Add-Content ${PORTABLE_PATH}/arduino-cli.yaml          ${STMicroelectronics_INDEX}
    Add-Content ${PORTABLE_PATH}/arduino-cli.yaml '    ]'
    Add-Content ${PORTABLE_PATH}/arduino-cli.yaml 'daemon:'
    Add-Content ${PORTABLE_PATH}/arduino-cli.yaml '    port: "50051"'
    Add-Content ${PORTABLE_PATH}/arduino-cli.yaml 'directories:'
    Add-Content ${PORTABLE_PATH}/arduino-cli.yaml '    data: ' -NoNewLine
    Add-Content ${PORTABLE_PATH}/arduino-cli.yaml             ${PORTABLE_PATH}
    Add-Content ${PORTABLE_PATH}/arduino-cli.yaml '    downloads: ' -NoNewLine
    Add-Content ${PORTABLE_PATH}/arduino-cli.yaml             ${PORTABLE_PATH}/staging
    Add-Content ${PORTABLE_PATH}/arduino-cli.yaml '    user: ' -NoNewLine
    Add-Content ${PORTABLE_PATH}/arduino-cli.yaml             ${PORTABLE_PATH}/Workspace
    Add-Content ${PORTABLE_PATH}/arduino-cli.yaml 'logging:'
    Add-Content ${PORTABLE_PATH}/arduino-cli.yaml '    file: ""'
    Add-Content ${PORTABLE_PATH}/arduino-cli.yaml '    format: text'
    Add-Content ${PORTABLE_PATH}/arduino-cli.yaml '    level: info'
    Write-Host "Line $(CurrentLine)  Editing                      arduino-cli.yaml Completed";
    Write-Host "Line $(CurrentLine)  Leaving                      install_arduino_cli_config";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}
#---------------------------------------------------------
# install_arduino_cli
#---------------------------------------------------------
function install_arduino_cli
{
    Write-Host "Line $(CurrentLine)  Entering                     install_arduino_cli";
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${PORTABLE_PATH})";
    if (Test-Path ${PORTABLE_PATH})
    {
        Write-Host "Line $(CurrentLine)  portable folder              Exists";   
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Executing                    md ${PORTABLE_PATH}";
        md ${PORTABLE_PATH}
    }
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${BASE_PATH}/dload/${ARDUINO_CLI_TB_NAME})";
    if (Test-Path ${BASE_PATH}/dload/${ARDUINO_CLI_TB_NAME})
    {
        Write-Host "Line $(CurrentLine)  arduino-cli                  Exists";   
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Downloading                  ${BASE_PATH}/dload/${ARDUINO_CLI_TB_NAME})";
        Write-Host "Line $(CurrentLine)  Executing                    Invoke-WebRequest -Uri ${ARDUINO_CLI_URI} -Outfile ${BASE_PATH}/dload/${ARDUINO_CLI_TB_NAME}";
        Invoke-WebRequest -Uri ${ARDUINO_CLI_URI} -Outfile ${BASE_PATH}/dload/${ARDUINO_CLI_TB_NAME}
    }
    Write-Host "Line $(CurrentLine)  Executing                    tar -xf ${BASE_PATH}/dload/${ARDUINO_CLI_TB_NAME} -C ${PORTABLE_PATH}";
    tar -xf ${BASE_PATH}/dload/${ARDUINO_CLI_TB_NAME} -C ${PORTABLE_PATH}

    Write-Host "Line $(CurrentLine)  Leaving                      install_arduino_cli";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# install_openocd_cfg_scripts
#---------------------------------------------------------
function install_openocd_cfg_scripts
{
    Write-Host "Line $(CurrentLine)  Entering                     install_openocd_cfg_scripts";
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${BASE_PATH}/dload/${OPENOCD_CFG_SCRIPT_NAME})";
    if (Test-Path ${BASE_PATH}/dload/${OPENOCD_CFG_SCRIPT_NAME})
    {
        Write-Host "Line $(CurrentLine)  openocd_cfg_scripts          Exists";   
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Downloading                  ${BASE_PATH}/dload/${OPENOCD_CFG_SCRIPT_TB_NAME}";
        Write-Host "Line $(CurrentLine)  Executing                    'Invoke-WebRequest -Uri ${OPENOCD_CFG_SCRIPT_URI} -Outfile ${BASE_PATH}/dload/${OPENOCD_CFG_SCRIPT_NAME}'";
        Invoke-WebRequest -Uri ${OPENOCD_CFG_SCRIPT_URI} -Outfile ${BASE_PATH}/dload/${OPENOCD_CFG_SCRIPT_NAME}
    }

    Write-Host "Line $(CurrentLine)  Executing                    tar -xf ${BASE_PATH}/dload/${OPENOCD_CFG_SCRIPT_NAME} -C ${BASE_PATH}/dload/";
    tar -xf ${BASE_PATH}/dload/${OPENOCD_CFG_SCRIPT_NAME} -C ${BASE_PATH}/dload/
    Write-Host "Line $(CurrentLine)  Executing                    Copy-Item -Recurse -Force -path: "${BASE_PATH}/dload/${OPENOCD_CFG_SCRIPT_NAME}-${OPENOCD_CFG_RELEASE}/*" -destination: ${BASE_PATH}/openocd/";
    Copy-Item -Recurse -Force -path: "${BASE_PATH}/dload/${OPENOCD_CFG_SCRIPT_NAME}-${OPENOCD_CFG_RELEASE}/*" -destination: ${BASE_PATH}/openocd/


    Write-Host "Line $(CurrentLine)  Leaving                      install_openocd_cfg_scripts";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# install_openocd-${OPENOCD_RELEASE}
#---------------------------------------------------------
function install_openocd-${OPENOCD_RELEASE}
{
    Write-Host "Line $(CurrentLine)  Entering                     install_openocd-${OPENOCD_RELEASE}";
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${BASE_PATH}/dload/${OPENOCD_TB_NAME})";
    if (Test-Path ${BASE_PATH}/dload/${OPENOCD_TB_NAME})
    {
        Write-Host "Line $(CurrentLine)  xpack-openocd                Exists";   
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Downloading                  ${BASE_PATH}/dload/${OPENOCD_TB_NAME}";
        Write-Host "Line $(CurrentLine)  Executing                    'Invoke-WebRequest -Uri ${OPENOCD_TB_URI} -Outfile ${BASE_PATH}/dload/${OPENOCD_TB_NAME}'";
        Invoke-WebRequest -Uri ${OPENOCD_TB_URI} -Outfile ${BASE_PATH}/dload/${OPENOCD_TB_NAME}
    }
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${BASE_PATH}/openocd)";
    if (Test-Path ${BASE_PATH}/openocd)
    {
        Write-Host "Line $(CurrentLine)  openocd                      is installed  removing it";
        Write-Host "Line $(CurrentLine)  Executing                    Remove-item -Recurse -Force ${BASE_PATH}/openocd ";
        Remove-Item -Recurse -Force ${BASE_PATH}/openocd
    }

    Write-Host "Line $(CurrentLine)  Executing                    tar -xf ${BASE_PATH}/dload/${OPENOCD_TB_NAME} -C ${BASE_PATH}/dload";
    tar -xf ${BASE_PATH}/dload/${OPENOCD_TB_NAME} -C ${BASE_PATH}
    Write-Host "Line $(CurrentLine)  Renaming                     ${BASE_PATH}/xpack-openocd-${OPENOCD_RELEASE}";
    Write-Host "Line $(CurrentLine)  To                           ${BASE_PATH}/openocd";
    Rename-Item  ${BASE_PATH}/xpack-openocd-${OPENOCD_RELEASE} ${BASE_PATH}/openocd

    Write-Host "Line $(CurrentLine)  Leaving                      install_openocd-${OPENOCD_RELEASE}";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# install_arduino-libraries
#---------------------------------------------------------
function install_arduino-libraries
{
    Write-Host "Line $(CurrentLine)  Entering                     install_arduino-libraries";

    Write-Host "Line $(CurrentLine)  Downloading                  ${BASE_PATH}/dload/ARDUINO_LIBRARIES";
    Write-Host "Line $(CurrentLine)  Executing                    Invoke-WebRequest -Uri ${ARDUINO_LIBRARIES_ZIP_URI} -Outfile ${BASE_PATH}/dload/${ARDUINO_LIBRARIES_ZIP_FILE}";
    Invoke-WebRequest -Uri ${ARDUINO_LIBRARIES_ZIP_URI} -Outfile ${BASE_PATH}/dload/${ARDUINO_LIBRARIES_ZIP_FILE}
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${$ARDUINO_PATH}/libraries)";
    if (Test-Path ${ARDUINO_PATH}/libraries)
    {
        Write-Host "Line $(CurrentLine)  libraries folder            Exists , Deleting";
        Remove-Item -Recurse -Force ${ARDUINO_PATH}/libraries
    }	
    Write-Host "Line $(CurrentLine)  Executing                    Expand-Archive -PATH ${BASE_PATH}/dload/${ARDUINO_LIBRARIES_ZIP_FILE} -Destination ${ARDUINO_PATH}/";
    Expand-Archive -Force -PATH ${BASE_PATH}/dload/${ARDUINO_LIBRARIES_ZIP_FILE} -Destination ${ARDUINO_PATH}/ 
    Write-Host "Line $(CurrentLine)  Renaming                     ${ARDUINO_PATH}/${ARDUINO_LIBRARIES_PREFIX}-${ARDUINO_LIBRARIES_RELEASE}";
    Write-Host "Line $(CurrentLine)  To                           ${PORTABLE_PATH}/libraries";
    Rename-Item ${ARDUINO_PATH}/${ARDUINO_LIBRARIES_PREFIX}-${ARDUINO_LIBRARIES_RELEASE} ${ARDUINO_PATH}/libraries
 
    Write-Host "Line $(CurrentLine)  Leaving                      install_arduino-libraries";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}


#---------------------------------------------------------
# install_arduino-${VERSION}
#---------------------------------------------------------
function install_arduino-${VERSION}
{
    Write-Host "Line $(CurrentLine)  Entering                        install_arduino-${VERSION}";
    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${BASE_PATH}/dload/${ARDUINO_ZIP_FILE})";
    if (Test-Path ${BASE_PATH}/dload/${ARDUINO_ZIP_FILE})
    {
        Write-Host "Line $(CurrentLine)  arduino-${ARDUINO_VERSION}.zip              Exists" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Downloading                     ${BASE_PATH}/dload/${ARDUINO_ZIP_FILE}";
        Invoke-WebRequest -Uri ${ARDUINO_ZIP_URI} -Outfile ${BASE_PATH}/dload/${ARDUINO_ZIP_FILE}
    }
    Write-Host "Line $(CurrentLine)  Executing                       tar -xf ${BASE_PATH}/dload/${ARDUINO_ZIP_FILE} -C ${BASE_PATH}/dload";
    tar -xf ${BASE_PATH}/dload/${ARDUINO_ZIP_FILE} -C ${BASE_PATH}
    Write-Host "Line $(CurrentLine)  Leaving                         install_arduino-${VERSION}";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}
#---------------------------------------------------------
# create_${BASE_PATH}
#---------------------------------------------------------
function create_${BASE_PATH}
{
    Write-Host "Line $(CurrentLine)  Entering                        create_${BASE_PATH}";
    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${BASE_PATH})";
    if (Test-Path ${BASE_PATH})
    {
        Write-Host "Line $(CurrentLine)  Folder                          ${BASE_PATH}  Exists" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Creating                       ${BASE_PATH}";
        Write-Host "Line $(CurrentLine)  Executing                      md ${BASE_PATH}";
        Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------";
        md ${BASE_PATH}
        Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------";
    }
    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${BASE_PATH})/dload";
    if (Test-Path ${BASE_PATH}/dload)
    {
        Write-Host "Line $(CurrentLine)  Folder                          ${BASE_PATH}/dload  Exists" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Creating                        ${BASE_PATH}/dload";
        Write-Host "Line $(CurrentLine)  Executing                       md ${BASE_PATH}/dload";
        md ${BASE_PATH}/dload
    }
    Write-Host "Line $(CurrentLine)  Executing                       cd ${BASE_PATH}";
    cd ${BASE_PATH}
    Write-Host "Line $(CurrentLine)  Current Directory             = $PWD";
    Write-Host "Line $(CurrentLine)  Leaving                         create_${BASE_PATH}";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# Echo Args
#---------------------------------------------------------
function echo_args
{
$MyVariable = 1

    Write-Host "Line $(CurrentLine)  Entering                        echo_args";
    Write-Host "Line $(CurrentLine)  OS                            = ${OS}";
    Write-Host "Line $(CurrentLine)  OS_LC                         = ${OS_LC}";
    Write-Host "Line $(CurrentLine)  USER                          = ${USER}";
    Write-Host "Line $(CurrentLine)  BASE_ROOT                     = ${BASE_ROOT}";
    Write-Host "Line $(CurrentLine)  INSTALL_FOLDER                = ${INSTALL_FOLDER}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_VERSION               = ${ARDUINO_VERSION}";
    Write-Host "Line $(CurrentLine)  ARDUINO_LIBRARIES_RELEASE     = $ARDUINO_LIBRARIES_RELEASE";	
    Write-Host "Line $(CurrentLine)  ARDUINO_CLI_RELEASE           = ${ARDUINO_CLI_RELEASE}";            
    Write-Host "Line $(CurrentLine)  BOARD_VENDOR _EI              = ${BOARD_VENDOR_EI}";            
    Write-Host "Line $(CurrentLine)  BOARD_VENDOR _EI_LC           = ${BOARD_VENDOR_EI_LC}";            
    Write-Host "Line $(CurrentLine)  BOARD_VENDOR _ST              = ${BOARD_VENDOR_ST}";            
    Write-Host "Line $(CurrentLine)  SKETCHBOOK_RELEASE            = ${SKETCHBOOK_RELEASE}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_RELEASE               = ${OPENOCD_RELEASE}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_CFG_SCRIPT_NAME       = ${OPENOCD_CFG_SCRIPT_NAME}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_CFG_RELEASE           = ${OPENOCD_CFG_RELEASE}";            
    Write-Host "Line $(CurrentLine)  XPACK_GCC_VERSION             = ${XPACK_GCC_VERSION}";            
    Write-Host "Line $(CurrentLine)  EICON_GCC_VERSION             = ${EICON_GCC_VERSION}";            
    Write-Host "Line $(CurrentLine)  CMSIS_VERSION                 = ${CMSIS_VERSION}";            
    Write-Host "Line $(CurrentLine)  CORE_NAME                     = ${CORE_NAME}";            
    Write-Host "Line $(CurrentLine)  CORE_PLATFORM_NAME            = ${CORE_PLATFORM_NAME}";            
    Write-Host "Line $(CurrentLine)  CORE_RELEASE                  = ${CORE_RELEASE}";            
    Write-Host "Line $(CurrentLine)  CPU                           = ${CPU}";            
    Write-Host "Line $(CurrentLine)  SKETCH                        = ${SKETCH}";            
    Write-Host "Line $(CurrentLine)";
    Write-Host "Line $(CurrentLine)  ARDUINO_TB_TYPE               = ${ARDUINO_TB_TYPE}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_TB_TYPE               = ${OPENOCD_TB_TYPE}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_CLI_TB_TYPE           = ${ARDUINO_CLI_TB_TYPE}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_LIBRARIES_PREFIX      = ${ARDUINO_LIBRARIES_PREFIX}";            
    Write-Host "Line $(CurrentLine)  SKETCHBOOK_NAME_PREFIX        = ${SKETCHBOOK_NAME_PREFIX}";            
    Write-Host "Line $(CurrentLine)  XPACK_GCC_NAME                = ${XPACK_GCC_NAME}";            
    Write-Host "Line $(CurrentLine)  EICON_GCC_NAME                = ${EICON_GCC_NAME}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_CLI_TB_NAME           = ${ARDUINO_CLI_TB_NAME}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_CFG_SCRIPT_NAME       = ${OPENOCD_CFG_SCRIPT_NAME}";            
    Write-Host "Line $(CurrentLine)";
    Write-Host "Line $(CurrentLine)  BASE_PATH                     = ${BASE_PATH}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_PATH                  = ${ARDUINO_PATH}";            
    Write-Host "Line $(CurrentLine)  PORTABLE_PATH                 = ${PORTABLE_PATH}";            
    Write-Host "Line $(CurrentLine)  HARDWARE_PATH_EI              = ${HARDWARE_PATH_EI}";            
    Write-Host "Line $(CurrentLine)  HARDWARE_PATH_ST              = ${HARDWARE_PATH_ST}";            
    Write-Host "Line $(CurrentLine)  TOOLS_PATH_EI                 = ${TOOLS_PATH_EI}";            
    Write-Host "Line $(CurrentLine)  TOOLS_PATH_ST                 = ${TOOLS_PATH_ST}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_PATH                  = ${OPENOCD_PATH}";            
    Write-Host "Line $(CurrentLine)  DLOAD_PATH                    = ${DLOAD_PATH}";            
    Write-Host "Line $(CurrentLine)  PREFS_PATH                    = ${PREFS_PATH}";            
    Write-Host "Line $(CurrentLine)  SKETCH_PATH                   = ${SKETCH_PATH}";            
    Write-Host "Line $(CurrentLine)";
    Write-Host "Line $(CurrentLine)  ARDUINO_ZIP_URI               = ${ARDUINO_ZIP_URI}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_CLI_URI               = ${ARDUINO_CLI_URI}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_TB_URI                = ${OPENOCD_TB_URI}";
    Write-Host "Line $(CurrentLine)  OPENOCD_CFG_SCRIPT_URI        = ${OPENOCD_CFG_SCRIPT_URI}";            
    Write-Host "Line $(CurrentLine)  SKETCHBOOK_ZIP_URI            = ${SKETCHBOOK_ZIP_URI}";            
    Write-Host "Line $(CurrentLine)  VARIANTS_URI                  = ${VARIANTS_URI}";            
    Write-Host "Line $(CurrentLine)  CORE_ZIP_URI                  = ${CORE_ZIP_URI}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_CORE_ZIP_NAME         = ${ARDUINO_CORE_ZIP_NAME}";            
    Write-Host "Line $(CurrentLine)";	
    Write-Host "Line $(CurrentLine)  VARIANTS_ZIP_NAME             = ${VARIANTS_ZIP_NAME} ";
    Write-Host "Line $(CurrentLine)  VARIANTS_NAME                 = ${VARIANTS_NAME} ";
    Write-Host "Line $(CurrentLine)  OPENOCD_TB_NAME               = ${OPENOCD_TB_NAME}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_TB_URI                = ${OPENOCD_TB_URI}";            
    Write-Host "Line $(CurrentLine)  STM32_INDEX                   = ${STM32_INDEX}";            
    Write-Host "Line $(CurrentLine)  STM32_INDEX                   = ${STM32_INDEX}";            
    Write-Host "Line $(CurrentLine)  STMicroelectronics_INDEX      = ${STMicroelectronics_INDEX}";            
    Write-Host "Line $(CurrentLine)";	
    Write-Host "Line $(CurrentLine)  ARDUINO_ZIP_FILE              = ${ARDUINO_ZIP_FILE}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_LIBRARIES_ZIP_FILE    = ${ARDUINO_LIBRARIES_ZIP_FILE}";            
    Write-Host "Line $(CurrentLine)  SKETCH_NAME                   = ${SKETCH_NAME}";                    
    Write-Host "Line $(CurrentLine)  SCRIPT_PATH                   = ${SCRIPT_PATH}";                    
    Write-Host "Line $(CurrentLine)  OPENOCD_Zip_FILE              = ${OPENOCD_Zip_FILE}";                    
    Write-Host "Line $(CurrentLine)  ARDUINO_CLI_ZIP_FILE          = ${ARDUINO_CLI_ZIP_FILE}";                    
    Write-Host "Line $(CurrentLine)  SKETCHBOOK_ZIP_FILE           = ${SKETCHBOOK_ZIP_FILE}";            
    Write-Host "Line $(CurrentLine)  SourceFileLocation            = ${SourceFileLocation}";            
    Write-Host "Line $(CurrentLine)  USER                          = ${USER}";            
    Write-Host "Line $(CurrentLine)  Leaving                         echo_args";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

#---------------------------------------------------------
# Launch_Arduino_IDE
#---------------------------------------------------------
function Launch_Arduino_IDE
{
    Write-Host "Line $(CurrentLine)  Entering                  Launch_Arduino_IDE"

    Write-Host "Line $(CurrentLine)  Executing                 $ARDUINO_PATH/arduino "
#Read-Host -Prompt "Pausing:  Press any key to continue"
    & $ARDUINO_PATH/arduino
    Write-Host "Line $(CurrentLine)  Leaving                   Launch_Arduino_IDE"
}

#---------------------------------------------------------
# CurrentLine
#---------------------------------------------------------
function CurrentLine
{
    $MyInvocation.ScriptLineNumber
}


#---------------------------------------------------------
# Main Entry point
#---------------------------------------------------------
Write-Host "Line $(CurrentLine)  Calling                      main()"
main
Write-Host "Line $(CurrentLine)  All Done  To exit Press Continue"
Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"

